package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

type fcblk struct { // file control block
	id	int
	file    *os.File
	name    string
	output  bool
	open    bool
	std	bool // true if stdin, stdout or stderr
	illegal bool
	eof     bool
	reclen  int
	writer  *bufio.Writer
	scanner  *bufio.Scanner
}

// scblk1 and scblk2 are memory indices of work areas used to reg[rc] = strings
// to the interpreter in the form of an scblk.

var (
	scblk0  int // null string
	scblk1  int
	scblk2  int
	fcbcount int
	fcblks  map[int]*fcblk
	fcb0    *fcblk // fcblk for stdin
	fcb1    *fcblk // fcblk for stdout
	fcb2    *fcblk // fcblk for stderr
	fcbrd *fcblk // fcblk for sysrd
	fcbn    int
)


func sysax__() {

	//fmt.Println("SYSAX")
	// Will need to mimic the swcoup function if want to use spitbol as unix filter
}

func sysbs__() {
	panik("sysbs not implemented.")
}

func sysbx__() {
	compiling = false
	traceinit()
	timeStart = time.Now()
}

func syscm__() {
	// syscm needed only if .ccmc conditional option selected
	panik("syscm not implemented.")
}

func sysdc__() {
	// SPITBOL now open open source, so no need to check expiry of trial version.
}

func sysdm__() {
	// No core dump for now.
}

func sysdt__() {
	t := time.Now()
	// TODO: Need to initialize scblk1 and scblk2 to blocks im mem[]
	s := t.Format(time.RFC822)
	reg[xl] = stringtoscblk(s,scblk0)
}

func sysea__() {
	//	fmt.Printf("sysea wa %v wb %v wc %v xr %v %v\n",
	//		reg[wa], reg[wb], reg[wc], reg[xr], reg[xl])
	// no action for now
	printscb(reg[xl])
}

func sysef__() {
	// sysef is eject file. sysen is endfile
}

func sysej__() {
	// close open files
	// need to find way to terminate program
	// reg[rc] = 999 to indicate end of job
	reg[rc] = 999
}

func sysem__() {
	var ok bool
	if reg[wa] > len(errorMessage) {
		// reg[rc] = null string if error number out of range
		reg[xr] = scblk0
		mem[scblk0] = 0
		ok = false
	}
	message, ok := errorMessage[reg[wa]]
	if !ok {
		fmt.Println("no message for id", reg[wa])
	}
	if ok {
		reg[xr] = stringtoscblk(message,scblk1)
	} else {
		fmt.Println("not ok for sysem")
		mem[scblk1+sclen] = 0 // reg[rc] = null string
		reg[xr] = scblk1
	}
}

func sysen__() {
	if reg[wa] == 0 {
		reg[rc] =  1
		return
	}
	fcb := fcblks[reg[wa]]
	if fcb == nil {
		reg[rc] = 1
	} else if fcb.std { 
		fcb.open = false
		return
	} else if !fcb.open {
		reg[rc] = 3
	} else {
		fcb.open = false
		if fcb.file.Close() != nil {
			reg[rc] = 3
		}
		delete(fcblks, reg[wa])
	}
}

func sysep__() {
	os.Stdout.WriteString("\n")
}

func sysex__() {
	// call to external function not supported
	reg[rc] = 1
}

func sysfc__() {
	var fcb	*fcblk
	/* cases

	1. both null
	case of both null should not occur, so reg[rc] = error 1 if it does.

	2. filearg1 null, filearg3 non-null.
	No action needed.


	3: filearg1 non-null, filearg2 null.
	We are just adding new variable assocation. Must check that 
	same mode, reg[rc] =ing error
	if not. Must have been provided a fcb, and reg[rc] = it.

	4. both non-null
	See if channel in use, reg[rc] error if so,then initialize channel.
	*/
	//fmt.Println("enter scb xl", reg[xl], "xr", reg[xr], "wa", reg[wa], "wb", reg[wb], "wc", reg[wc])

	//s1 := scblktostring(reg[xl])
	//fmt.Println("arg1", s1)
	////s2 := scblktostring(reg[xr])
	//fmt.Println("arg2", s2)
	//var scblk1,scblk2 []int

	fcbn := reg[wa]
	reg[wa] = 0	
	// pop any stacked arguments, since we ignore them (for now).
	reg[xs] += reg[wc]
/*
	if second argument is null, then call is just to check 
	third argument,  in case it is needed. 
	we don't need it for this stage of implementation.
*/
	scb1 := scblktoslice(reg[xl])
	scb2 := scblktoslice(reg[xr])
	len1 := len(scb1)
	len2 := len(scb2)
	switch {

	case len1 == 0 && len2 == 0:
		reg[rc] = 1

	case len1 == 0 && len2 > 0:
		reg[wa] = 0
		reg[xl] = 0
		reg[wc] = 0

	case len1 > 0 && len2 == 0:
		// error if no fcb available
		if fcbn == 0 {
			reg[rc] = 1
		}
		fcb = fcblks[fcbn]
		if fcb == nil {
			reg[rc] = 1
		}
		if fcb.output && reg[wb] == 0 || !fcb.output && reg[wb] != 0 {
			fcb.illegal = true // sysio call will signal error
		}

	case len1 > 0 && len2 > 0:

		fcb = newfcb(scblktostring(reg[xr]),false)
		fcb.output = reg[wb] > 0
		reg[xl] = fcbcount // reg[rc] = private fcblk index
		reg[wa] = 0
		reg[wc] = 0
	}
	//fmt.Println("sysfcb")
	//listfcb(fcb)
	//listregs()
}

func sysgc__() {
 /*
	if true {
		fmt.Println("sysgc", reg[xr])
		fmt.Println("sysgc dnamb - beginning", reg[wa])
		fmt.Println("sysgc dnamp - next avail", reg[wb])
		fmt.Println("sysgc dname - last avail + 1", reg[wc])
	}
*/
	// no action needed
}

func syshs__() {
	reg[rc] = 1
}

func sysid__() {
	s := ""
	reg[xr] = stringtoscblk(s,scblk1)
	printscb(scblk1)
	s, err := os.Hostname()
	check(err)
	// empty scblk2 for now 
	mem[scblk2+sclen] = 0
	//reg[xl] = stringtoscblk(s,scblk2)
	reg[xl] = scblk2
}

func sysif__() {
	// TODO: support include files.
	reg[rc] = 1
}

func sysil__() {
	reg[wa] = maxLineLength
	reg[wc] = 1 // text file
}

func sysin__() {
	if reg[wa] == 0 {
		panik("sysin wa == 0")
	}
	fcb := fcblks[reg[wa]]
	if !fcb.std {
		r := open(fcb)
			if r > 0 {
				fmt.Println("cannot open",fcb.name)
				reg[rc] = 1
				return
		}
	}
	b := fcb.scanner.Scan()
	if b {
		s := fcb.scanner.Text()
		r := runecount(s)
		if reg[wc] > r {
			reg[wc] = r
		}
		_ = stringtoscblk(s,reg[xr])
		return
	} else {
		//fmt.Println("sysin scanner failed")
		//err := fcb.scanner.Err()
		//fmt.Println("error",err)
		//panik("crash")
		reg[rc] = 1
	}
}

func sysio__() {
	if reg[wa] == 0 {
		reg[rc] = 1
		return
	}
	fcb := fcblks[reg[wa]]
	if fcb == nil {
		reg[rc] = 1
	}
	//listfcb(fcb)
	r := open(fcb)
	if r != 0 {
		reg[rc] = 1
		return
	}
	reg[xl] = reg[wa]
	reg[wc] = maxLineLength
	//listfcb(fcb)
}

func sysld__() {
	reg[rc] = 1
}

func sysmm__() {

moremem:
	oldlen := memLast
	newlen := oldlen + 100000
	if newlen >= cap(mem) {
		// no memory available for expansion
		var newmem []int
		newcap := 2 * cap(mem)
		newmem = make([]int, newlen, newcap)
		copy(newmem[:oldlen], mem[0:oldlen])
		goto moremem
	} else {
		memLast = newlen
		reg[xr] = newlen - oldlen
		fmt.Println("sysmm old", oldlen, "new", newlen)
	}
}

func sysmx__() {
	// reg[rc] = default
	reg[wa] = 0
}

func sysou__() {
	// wa is 0 for terminal output, 1 for stdout output, else has fcb id
	var fcb *fcblk
	if reg[wa] == 0 {
		fcb = fcb2
	} else if reg[wa] == 1 {
		fcb = fcb1
	} else {
		fcb = fcblks[reg[wa]]
		if fcb == nil {
			reg[rc] = 2
			return
		}
	}
	writeline(fcb, mem[reg[xr]+sclen],reg[xr])
	if reg[rc] == 1 {
		reg[rc] = 2 // indicate i/o error
	}
}
func syspi__() {
	writeline(fcb2, reg[wa],reg[xr])
	if reg[rc] == 1 {
		reg[rc] = 2 // indicate i/o error
	}
}

func syspl__() {
}

func syspp__() {
	reg[wa] = 100
	reg[wb] = 60
	reg[wc] =  0// err copy, std printer to interactive channel
}

var syspr_errors int

func syspr__() {
	writeline(fcb1, reg[wa],reg[xr])
	// reg[rc] may have been set to indicate i/o errori
}

var (
	sysrds int

//	ifile   *os.File
//	scanner *bufio.Scanner
)

var sysrdeof bool
var sysrdcalls int

func sysrd__() {
	//	read record from standard input file

	//		(XR)	pointer to scblk
	//		(WC)	length of scblk in characters
	//		JSR	SYSRD
	//		PPM	loc
	//			endfile or no i/p file after SYSXI or
	//			input file name change.  If the former,
	//			scblk length is zero. If file name change,
	//			length is non-zero, caller should re-issue
	//			sysrd to obtain input record,

	if sysrdeof {
		reg[rc] = 1
		mem[reg[wa]+sclen] = 0
		return
	}
	if !fcbrd.open {
		r := open(fcbrd)
		if r != 0 {
			reg[rc] = 1
		}
	}
	sysrdcalls++
	if sysrdeof {
		reg[rc] = 1
		mem[reg[wa]+sclen] = 0
		return
	}
	b := fcbrd.scanner.Scan()
	if b {
			s := fcbrd.scanner.Text()
	//	fmt.Println("sysrd text read",s)
		r := runecount(s)
		if reg[wc] > r {
			reg[wc] = r
		}
		//fmt.Println("SYSRD",s)
		reg[xr] = stringtoscblk(s,reg[xr])
		return
	} else {
		//fmt.Println("sysrd scanner failed")
		//err := fcbrd.scanner.Err()
		//fmt.Println("error",err)
		//panik("crash")
		sysrdeof = true
		reg[rc] = 1
}
}

func sysri__() {
	//readline(fcb0, reg[xr], mem[reg[xr]+sclen])
}

func sysrw__() {
	reg[rc] = 2
}

func sysst__() {
	reg[rc] = 5
}

func systm__() {
/*
	d := time.Since(timeStart)
	IA = int(d.Nanoseconds() / 1000000)
*/
}

func systt__() {
	// No trace for now
	panik("systt not implemented.")
}

func sysul__() {
}

func sysxi__() {
	reg[rc] = 1
}

func syscall(ea int) {

	//	fmt.Println("syscall ", sysName[ea])

	switch ea {

	case sysax_:
		sysax__()

	case sysbs_:
		sysbs__()

	case sysbx_:
		sysbx__()

	case syscm_:
		syscm__()

	case sysdc_:
		sysdc__()

	case sysdm_:
		sysdm__()

	case sysdt_:
		sysdt__()

	case sysea_:
		sysea__()

	case sysef_:
		sysef__()

	case sysej_:
		sysej__()

	case sysem_:
		sysem__()

	case sysen_:
		sysen__()

	case sysep_:
		sysep__()

	case sysex_:
		sysex__()

	case sysfc_:
		sysfc__()

	case sysgc_:
		sysgc__()

	case syshs_:
		syshs__()

	case sysid_:
		sysid__()

	case sysif_:
		sysif__()

	case sysil_:
		sysil__()

	case sysin_:
		sysin__()

	case sysio_:
		sysio__()

	case sysld_:
		sysld__()

	case sysmm_:
		sysmm__()

	case sysmx_:
			sysmx__()

	case sysou_:
		sysou__()

	case syspi_:
		syspi__()

	case syspl_:
		syspl__()

	case syspp_:
		syspp__()

	case syspr_:
		syspr__()

	case sysrd_:
		sysrd__()

	case sysri_:
		sysri__()

	case sysrw_:
		sysrw__()

	case sysst_:
		sysst__()

	case systm_:
		systm__()

	case systt_:
		systt__()

	case sysul_:
		sysul__()

	case sysxi_:
		sysxi__()

	}
}

// Utility functions

func check(e error) {
	if e != nil {
		panik("check error")
	}
}

func systest() {

	// test scblk/string procedures

	s := "test"
	fmt.Println("systest string", s)
	fmt.Println("rune count", runecount(s))

	r1 := stringtorunes(s)
	s2 := runestostring(r1)
	fmt.Println("string to runes/back to string", s2)

	stringtoscblk(s,scblk1)
	s3 := scblktostring(scblk1)
	fmt.Println("string/scblk and back", s3)
}

func printscb(loc int) {
	s := scblktostring(loc)
	fmt.Println(s)
}

func listfcb(fcb *fcblk) {
	if fcb == nil {
		panik("null fcb")
	}
	fmt.Println("fcb id",fcb.id,"name",fcb.name,"std",fcb.std,"output",
		fcb.output, "open",fcb.open,"eof",fcb.eof,"reclen",fcb.reclen,
		"scanner", fcb.scanner!=nil,"writer",fcb.writer!=nil)
}

func runecount(s string) int {
	var n int
	for range s {
		n++
	}
	return n
}

func runestostring(runes []rune) string {
	return string(runes)
}

func scblktoslice(loc int) []int {
	n := mem[loc+sclen]
	return mem[loc : loc+cfp_f+n]
}

func scblktorunes(loc int) []rune {
	n := mem[loc+sclen]
	ints := mem[loc+cfp_f : loc+cfp_f+n]
	runes := make([]rune, n)
	for i := 0; i < n; i++ {
		runes[i] = rune(ints[i])
	}
	return runes
}
func scblktostring(loc int) string {

	//	Convert scblk starting with index id to Go string

	r := scblktorunes(loc)
	s := string(r)
	return s
}

func stringtorunes(s string) []rune {
	r := []rune(s)
	r = r[0:len(r)] // make slice so can adjust length lower later
	return r
}

func stringtoscblk(s string,loc int) int {

	// Populate scblk starting at locaton loc with runes in string s

	n := runecount(s)
	r := []int32(s)
	if n > len(r) {
		n = len(r)
	}
	if n > maxLineLength {
		n = maxLineLength
	}
	mem[loc+sclen] = n
	scb := scblktoslice(loc)


	for i := 0; i < n; i++ {
		scb[cfp_f+i] = int(r[i])
	}
	return loc
}

func writeline(fcb *fcblk,n int,loc int) {
	var err error
	// loc gives scblk address
	// n is number of characters to write
	//fmt.Println("writeline n",n,"loc",loc)
	if n > 0 {
		for i:=0; i<n; i++ {
			_,err = fmt.Fprintf(fcb.file,"%c",
				rune(mem[loc+cfp_f+i]))
				if err != nil {
					reg[rc] = 1
				}
		}
	}
	_,err = fmt.Fprintf(fcb.file,"%c",'\n')
	if err != nil {
		reg[rc] = 1
	}
}


func open(fcb *fcblk) int{
	//fmt.Println("enter open")
	//listfcb(fcb)
	if fcb.open {
		return 0
	}
	if fcb.output {
		file, err := os.Create(fcb.name)
		if err != nil {
			fmt.Println("create failed",fcb.name)
			return 1
		}
		fcb.file = file
		fcb.writer = bufio.NewWriter(file)
	} else {
		file, err := os.Open(fcb.name)
		if err != nil {
			fmt.Println("open failed",fcb.name)
			return 1
		}
		fcb.file = file
		fcb.scanner = bufio.NewScanner(file)
	}
	fcb.open = true
	//fmt.Println("exit open")
	//listfcb(fcb)
	return 0
}

func closefiles() {
	for _, fcb := range fcblks {
		if fcb.std {
			continue
		}
		if fcb.open {
			//fmt.Println("start closing file")
			//listfcb(fcb)
			fcb.file.Close()
			fcb.open = false
			//fmt.Println("finis closing file")
			//listfcb(fcb)
		}
	}
}
func newfcb(name string,out bool) *fcblk {
	fcbcount++
	fcb := new(fcblk)
	fcblks[fcbcount] = fcb
	fcb.id = fcbcount
	fcb.name = name
	fcb.output = out
	//fmt.Println("newfcb")
	//listfcb(fcb)
	return fcb
}
func sysinit() {
	// fcb id's begin with three to avoid confusion with unix file
	// descriptor numbers for srtdin (0), stdout(1), stderr(2)
	fcbcount = 3
	fcblks = make(map[int]*fcblk)
	fcb0 = newfcb("dev/stdin",false)
	fcb0.id = 0
	fcb0.open = true
	fcb0.file = os.Stdin
	fcb0.file = os.Stdin
	fcb0.scanner = bufio.NewScanner(fcb0.file)
	fcb0.std = true

	fcb1 = newfcb("dev/stdout",true)
	fcb1.id = 1
	fcb1.file = os.Stdout
	fcb1.open = true
	fcb1.std = true
	fcb1.writer = bufio.NewWriter(fcb1.file)

	fcb2 = newfcb("dev/stderr",true)
	fcb2.id = 2
	fcb2.file = os.Stderr
	fcb2.std = true
	fcb2.open = true
	fcb2.writer = bufio.NewWriter(fcb0.file)

	fcbrd = newfcb(inFilename,false)
}
