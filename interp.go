package main

import (
	//"fmt"
	"bufio"
	"flag"
	"fmt"
	"math"
	"os"
)

const (
	op_ = 24
	dst_= 20
	src_= 16
	off_ = 0
	op_m = 2^8 - 1
	dst_m = 2^4 - 1
	src_m = 2^4 - 1
	off_m = 2^15 - 1
	maxLineLength = 120
	stackLength  = 1000
	numops = zrb + 1
)
var (
	compiling	bool = true // execution program, after compilatio
	tracing		bool // set if tracing
	depth        int    = 2
	blanks       string = "                                                   "
	hdr          string = " "
	interpError  bool
	pclast       int
	mem          []int
	startData    int // start of data area
	stackEnd     int
	memLast      int // index of last allocated memory word
	long1, long2 int64
	uint1, uint2  uint32
	prcstack      [32]int
	inst          int
	loc,op,dst,src,off,stmt int
	overflow      bool
	f1, f2        float32
	d1            float64
	inFilename    string
	inFile        *os.File
	sysrdScanner  *bufio.Scanner
	sysrdReader   *bufio.Reader

	instCount  int // number of instructions executed
	stmtCount  int // number of statements executed (stmt opcode)
	traceLines int // number of trace lines
	riscalCount int	// number of riscal statements executed

	maxOffset int

	stackStart int
	opCount = make(map[int]int)

	reg[reg_count] int
)

func interp() {

	fmt.Println("start execution at label start")
	fmt.Println("start execution at location",reg[pc])
	fmt.Println(reg[pc], sourceCode[reg[pc]])

	for {

		if interpError {
			break
		}
	riscalCount++
	if riscalCount > slFlag {
		panik("too many riscal instructions")
	}
		if reg[pc] < s_aaa || reg[pc] > s_yyy+2 {
			fmt.Println("ip out of range ", reg[pc], "s_aaa", s_aaa, "s_ yyy", s_yyy+2)
			listregs()
			panik("ip out of range")
			interpError = true
			return
			break
		}

		instCount = instCount + 1
		if tiFlag || tsFlag {
			traceLines++
		}
		if stmtCount > slFlag {
			fmt.Println("too many statements", stmtCount)
			interpError = true
			return 
		}

		inst = mem[reg[pc]]
		op = inst >> op_ & op_m
		dst = inst >> dst_ &  dst_m
		src = inst >> src_ &  src_m
		off = inst >> off_ &  off_m

		opCount[op]++ // count number of times op used

		if tiFlag {
			listinst(reg[pc])
		} else if tsFlag && op == stmt {
			listinst(reg[pc])
		}
		pclast = reg[pc]
		reg[pc]++

		reg[pc] = start
		switch op {

		case nop:
			// do nothing

		// 1 - Basic Instruction Set

		case stmt:
			reg[r1] = 0
			reg[r2] = 0
			stmtCount++
			// cannot increment ip for stmt as that would mess up call/ret
		case ent:
		case exi:
			if teFlag {
				if reg[rc] > 0 {
					//hdr := blanks[1:depth]
					//fmt.Println(hdr, "prc:exit", prcName[off], reg[rc])
					fmt.Println("prc:exit", prcName[off], reg[rc])
				}
				depth -= 2
			}
		case mov:
			reg[dst] = reg[src]
		case lbx:
			loc = reg[src] - off
			if tiFlag {
				fmt.Println("LBX MEM", loc, mem[loc], pclast)
			}
			//
			reg[dst] = mem[loc]
			if loc >= c_aaa {
				reg[dst] = mem[loc]
			} else {
				fmt.Println("illegal load", loc, c_aaa, dname)
				interpError = true
				panik("illegal load")
				return
			}
		case lod:
			loc := reg[src] + off
			if tiFlag {
				fmt.Println("LOD MEM", loc, mem[loc], pclast)
			}
			//
			reg[dst] = mem[loc]
			if loc >= c_aaa {
				reg[dst] = mem[loc]
			} else {
				fmt.Println("illegal load", loc, c_aaa, dname)
				interpError = true
				panik("illegal load")
				return
			}
		case sbx:
			loc := reg[src] - off
			if (loc >= stackEnd && loc <= dname) || (loc >= c_yyy && loc <= s_aaa) || (loc >= dnamp && loc >= dname) {
				mem[loc] = reg[dst]
			} else {
				fmt.Println("illegal store", loc)
				fmt.Println("stackEnd", stackEnd, "c_yyy", c_yyy, "s_aaa", s_aaa)
				panik("bad store")
				listregs()
				return
			}
			if tiFlag {
				fmt.Println("STO MEM", loc, reg[dst], pclast)
			}
			
		case sto:
			loc := reg[src] + off
			if (loc >= stackEnd && loc <= dname) || (loc >= c_yyy && loc <= s_aaa) || (loc >= dnamp && loc >= dname) {
				mem[loc] = reg[dst]
			} else {
				fmt.Println("illegal store", loc)
				fmt.Println("stackEnd", stackEnd, "c_yyy", c_yyy, "s_aaa", s_aaa)
				panik("bad store")
				listregs()
				return
			}
			if tiFlag {
				fmt.Println("STO MEM", loc, reg[dst], pclast)
			}
		case bsw:
			reg[pc] = reg[pc] + (reg[dst])
		case brn:
			reg[pc] = off
		case bri:
			reg[pc] = reg[dst]
		case ldc:
			reg[dst] = off
		case lei:
			reg[dst] = mem[(reg[dst])-1]
		case ppm:
			/* ppm with no arguments is just nop */
		case jsr:
			reg[rc] = 0
			reg[xs]--
			mem[(reg[xs])] = reg[pc]
			if tiFlag {
				fmt.Println("reg[xs]", reg[xs])
			}
			if teFlag {
				//hdr := blanks[1:depth]
				//fmt.Println(hdr, "prc:jsr", prcName[off], reg[pc])
				fmt.Println("prc:jsr", prcName[off], reg[pc])
				depth += 2
			}
			reg[pc] = off
		case sys:
			reg[rc] = 0
			syscall(off)
			if teFlag {
				//hdr := blanks[1:depth]
				//fmt.Println(hdr, "prc:sys", sysName[off], reg[rc])
				fmt.Println("prc:sys", sysName[off], reg[rc])
			}
			if reg[rc] == 999 || reg[rc] == 999 {
				return // end execution
			}
 /*
		case brc:
			// branch on return code
			if reg[rc] == 0 {
				reg[pc] = reg[pc] + off
			} else {
				reg[pc] = reg[pc] + (reg[rc]) - 1
			}
 */
/*
		case ret:
			if teFlag {
				depth -= 2
			}
			reg[pc] = mem[reg[xs]]
			reg[xs]++
*/
		case err, erb:
			reg[wa] = off
			reg[pc] = err00
		case icv, ica:
			reg[dst]++
		case dcv, dca:
			reg[dst]--
		case mnz:
			reg[dst] = 1
		case zer:
			reg[dst] = 0

		// 2 - Operations on one word integer values (addresses)

		case add:
			reg[dst] += reg[src]
		case sub:
			reg[dst] -= reg[src]
		case beq, ceq:
			if reg[dst] == reg[src] {
				reg[pc] = off
			}
		case bge:
			if reg[dst] >= reg[src] {
				reg[pc] = off
			}
		case bgt:
			if reg[dst] > reg[src] {
				reg[pc] = off
			}
		case bne, cne:
			if reg[dst] != reg[src] {
				reg[pc] = off
			}
		case ble:
			if reg[dst] <= reg[src] {
				reg[pc] = off
			}
		case blt:
			if reg[dst] < reg[src] {
				reg[pc] = off
			}
		case blo:
			if reg[dst] < reg[src] {
				reg[pc] = off
			}
		case bhi:
			if reg[dst] > reg[src] {
				reg[pc] = off
			}
		case bnz:
			if reg[dst] != 0 {
				reg[pc] = off
			}
		case bze:
			if reg[dst] == 0 {
				reg[pc] = off
			}
		case lct:
			reg[dst] = reg[src]
		case bct:
			reg[dst]--
			if reg[dst] > 0 {
				reg[pc] = off
			}
		case aov:
			long1 = int64(reg[dst])
			if long1 > math.MaxUint32 {
				reg[pc] = off
			}
		case bev:
			if reg[dst]&1 == 0 {
				reg[pc] = off
			}
		case bod:
			if reg[dst]&1 != 0 {
				reg[pc] = off
			}

		// 3 - Operations on the code pointer register (CP)

		case lcp:
			reg[cp] = reg[dst]
		case scp:
			reg[dst] = reg[cp]
		case lcw:
			reg[dst] = mem[reg[cp]]
			reg[cp]++
		case icp:
			reg[cp]++

		// 4 - Operatons on signed integer values

		case ldi:
			reg[ia] = reg[dst]

		case adi:

			long1, long2 = int64(reg[ia]), int64(reg[dst])
			long1 += long2
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				overflow = true
			} else {
				overflow = false
				reg[ia] += reg[dst]
			}
		case mli:

			long1, long2 = int64(reg[ia]), int64(reg[dst])
			long1 *= long2
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				overflow = true
			} else {
				overflow = false
				reg[ia] *= reg[dst]
			}

		case sbi:

			long1, long2 = int64(reg[ia]), int64((reg[dst]))
			long1 -= long2
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				overflow = true
			} else {
				overflow = false
				reg[ia] -= reg[dst]
			}

		case dvi:

			if reg[dst] == 0 {
				overflow = true
			} else {
				overflow = false
				reg[ia] /= reg[dst]
			}

		case rmi:

			if reg[dst] == 0 {
				overflow = true
			} else {
				overflow = false
				reg[ia] %= reg[dst]
			}

		case sti:

			reg[r1] = reg[ia]

		case ngi:

			long1 = int64(reg[ia])
			long1 = -long1
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				overflow = true
			} else {
				overflow = false
				reg[ia] = -reg[ia]
			}

		case ino:

			if !overflow {
				reg[pc] = off
			}

		case iov:

			if overflow {
				reg[pc] = off
			}

		case ieq:

			if reg[ia] == 0 {
				reg[pc] = off
			}

		case ige:

			if reg[ia] >= 0 {
				reg[pc] = off
			}

		case igt:

			if reg[ia] > 0 {
				reg[pc] = off
			}

		case ile:

			if reg[ia] <= 0 {
				reg[pc] = off
			}

		case ilt:

			if reg[ia] < 0 {
				reg[pc] = off
			}

		case ine:

			if reg[ia] != 0 {
				reg[pc] = off
			}

			// 5 - Operationsn on real values

		case ldr:

 /*
			RA = float64(math.Float32frombits(uint32(r1)))
 */

		case str:

/*
			reg[r1] = int(math.Float32bits(float32(RA)))
*/
		case adr:
/*
			f1 = math.Float32frombits(uint32(reg[dst]))
			RA = RA + float64(f1)
*/
		case sbr:
/*
			f1 = math.Float32frombits(uint32(reg[dst]))
			RA = RA - float64(f1)
*/
		case mlr:
/*
			f1 = math.Float32frombits(uint32(reg[dst]))
			RA = RA * float64(f1)
*/

		case dvr:
/*
			f1 = math.Float32frombits(uint32(reg[dst]))
			RA = RA / float64(f1)
*/
		case rov:
/*
			if math.IsNaN(RA) || math.IsInf(RA, 0) {
				reg[pc] = off
			}
*/
		case rno:
/*
			if !(math.IsNaN(RA) || math.IsInf(RA, 0)) {
				reg[pc] = off
			}
*/
		case ngr:
/*
			RA = -RA
*/
		case req:
/*
			if RA == 0.0 {
				reg[pc] = off
			}
*/
		case rge:
/*
			if RA >= 0.0 {
				reg[pc] = off
			}
*/
		case rgt:
/*
			if RA > 0.0 {
				reg[pc] = off
			}
*/
		case rle:
/*
			if RA <= 0.0 {
				reg[pc] = off
			}
*/
		case rlt:
/*
			if RA < 0.0 {
				reg[pc] = off
			}
*/
		case rne:
/*
			if RA != 0.0 {
				reg[pc] = off
			}
*/
		// 6 - Operations on character values

		case plc, psc:
			reg[dst] = reg[dst] + reg[src] + cfp_f

		case cmc:

			s1 := mem[reg[xl]:]
			s2 := mem[reg[xr]:]
			n := reg[wa]
			for i := 0; i < n; i++ {
				if s1[i] < s2[i] {
					reg[pc] = reg[r1]
					break
				} else if s1[i] > s2[i] {
					reg[pc] = reg[r2]
					break
				}
			}
			reg[xl], reg[xr] = 0, 0

		case trc:

			n := reg[wa]
			ixl := reg[xl]
			ixr := reg[xr]
			for i := 0; i < n; i++ {
				mem[ixl+i] = mem[ixr+(mem[ixl+i])]
			}
		case flc:
			panik("flc not implemented")

		// 7 - Operations on bit string values

		case anb:
			reg[dst] &= reg[src]
		case orb:
			reg[dst] |= reg[src]
		case xob:
			reg[dst] ^= reg[src]
		case rsh:
			reg[dst] = reg[dst] >> off
		case lsh:
			reg[dst] = reg[dst] << off
		case nzb:
			if reg[dst] != 0 {
				reg[pc] = off
			}
		case zrb:
			if reg[dst] == 0 {
				reg[pc] = off
			}

			// 8 = Conversion  instructions

		case mti:
			reg[ia] = reg[dst]
		case mfi:
			long1 = int64(reg[ia])
			reg[r1] = reg[ia]
			if off != 0 {
				if long1 > math.MaxInt32 || long1 < 0 {
					reg[pc] = off
				}
			}
		// Min spec says reg[ia] destroyed, but code in icblk
		// indicates it should be preserved.
		// Code for x32 and x64 versions preserves reg[ia]
		//	reg[ia] = 0
		case itr:
/*
			RA = float64(reg[ia])
*/
		case rti:
/*
			long1 = int64(RA)
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				reg[pc] = off
			} else {
				reg[ia] = long1
			}
*/
		case ctw, ctb:
			reg[dst] = reg[dst] + off
		case cvm:
			long1 = int64(reg[ia])
			//	fmt.Println("CVM start",reg[ia],"reg[wb]",reg[wb])
			long1 = long1*10 - int64(reg[wb]-48)
			//		fmt.Println("CVM finis reg[ia]",long1)
			if long1 > math.MaxInt32 || long1 < math.MinInt32 {
				reg[pc] = off
			} else {
				reg[ia] = int(long1)
			}
		case cvd:

			r := reg[ia] % 10
			reg[ia] = reg[ia] / 10
			reg[wa] = int(-r + 48) // convert to char code for digit
		// 10 - Block move instructions

		case mvc, mvw:
			n := int(reg[wa])
			for i := 0; i < n; i++ {
				mem[reg[xr]+i] = mem[reg[xl]+i]
			}
			reg[xl] += reg[wa]
			reg[xr] += reg[wa]
		case mcb, mwb:
			for i := 0; i < int(reg[wa]); i++ {
				mem[int(reg[xr])-1-i] = mem[int(reg[xl])-1-i]
			}
			reg[xl] -= reg[wa]
			reg[xr] -= reg[wa]

			// 10 - Operations on the Stack

		case chk:
			if int(reg[xs]) < stackEnd+100 {
				reg[pc] = stako // branch to stack overflow section
			}
		default:
			fmt.Println("unknown opcode",op)
			panik("unknown opcode")
			return
		}

		if trFlag {
			listregs()
		}
		if reg[r0] != 0 {
			panik("r0 not zero at end of instruction")
			listregs()
			return
		}
	}
	return
}

func listinst(pc int) {
	if pc > s_aaa && pc <= s_yyy {
		if op == stmt {
			fmt.Println()
			fmt.Println("STMT",stmtCount,code[pc])
		} else {
			fmt.Println(code[pc])
		}
	} else {
		listregs()
		interpError = true
		//		fmt.Println("ip out of range",pc)
		panik("listinst - ip out of range")
	}
}

func listregs() {

	name := opName[op]
	if op == stmt {
		name = "STMT"
	}
	//fmt.Println("op", opName[op], "depth", depth, "ip", reg[pc], "r1", reg[r1], "r2", reg[r2],
	fmt.Println("op", name, "reg[pc]", reg[pc], "r1", reg[r1], "r2", reg[r2],
		"wa", reg[wa], "wb", reg[wb], "wc", reg[wc],
		"xl", reg[xl], "xr", reg[xr], "xs", stackStart-int(reg[xs]), mem[reg[xs]],
		"reg[ia]", reg[ia], "rc", reg[rc], "off", off)
	if reg[r0] != 0 {
		fmt.Println("r0 not zero!", reg[r0])
	}
}

func traceinit() {
	if compiling {
		if !tcFlag {
			tracing = false
			return
		}
	}
		
	if tiFlag {
		tracing = true
		tsFlag = true
	}
	if tiFlag || tsFlag {
		teFlag = true
		toFlag = true
		tracing = true
	}
	if tsFlag {
		tracing = true
		teFlag = true
	}
}

func startup() {

	inFilename = flag.Arg(0)
	fmt.Println("inFile ", inFilename)
	sysinit() // initialize osint

	traceinit()

	fmt.Println("statement limit", slFlag)

	fmt.Println("program length", len(code))
	// count number of instructions in program
	fmt.Println("number of instructions in program", codeLength)

	last := codeLength + 10
	stackEnd = last
	last += stackLength
	stackStart = last
	last += 30000 // reserve for size of largest table, array etc.
	startData = last

	//	program = nil // no longer needed since now in memory
	// scblk is just four words, sufficient to hold null string

	scblk0 = last
	scblk1 = last + cfp_f + maxLineLength
	scblk2 = scblk1 + maxLineLength + 1
	last += 3 * (maxLineLength + cfp_f)
	fmt.Println("startData", startData)

	///scannerStdin = bufio.NewScanner(os.Stdin)

	//mem = make([]int, 1<<20, 1<<23)
	mem = make([]int, 5000000, 5000000)
	fmt.Println("mem length ", len(mem))
	fmt.Println("mem capacity ", cap(mem))

	for i := range code{
		mem[i] = code[i]
	}

//	mem[r0] = mem[0] must be zero
	mem[r0] = 0

	// wa=initial stack pointer
	// xs = one past stack base (subtract 1 to get first stack entry)

	reg[wa] = int(stackStart)
	reg[xs] = int(stackStart - 1)

	reg[xr] = int(startData)    // address of first word in data area
	reg[xl] = int(len(mem) - 1) // address of last word in data area

	//fmt.Println("scblk0", scblk0, "scblk1", scblk1, "scblk2", scblk2)
	//fmt.Println("stackStart", stackStart, "stackEnd", stackEnd)
	//fmt.Println("start data ", reg[xr], " end data ", reg[xl])
	//fmt.Println("s_aaa", s_aaa, "s_yyy+2", s_yyy+2)

}

func panik(s string) {
	interpError = true
	fmt.Println("interpreter fatal error:", s)
	fmt.Println("ip", reg[pc])
	listregs()
	fmt.Println("last ip", pclast)
	listinst(pclast)
	//panic("pc out of range")
}


