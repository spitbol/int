package main

import (
	"flag"
	"fmt"
	"os"
	//"sort"
	"time"
)

var (
	ocFlag		bool // trace operator count
	slFlag		int  // riscal instruction count limit
	tcFlag		bool // trace during compilation
	teFlag		bool // trace procedure entry/exit
	tiFlag		bool // trace intstuction
	toFlag		bool // trace osint calls
	trFlag		bool // trace registers
	tsFlag		bool // trace statements

	timeStart 	time.Time //  starting time

// the registers ia and ra are used to store current values
// of the integer and real accumulators. the variables
// are used to make their values globally accessible.

	RA 		float64 // real accumulator
	IA            	int64 // integer accumulator
)

func main() {

	flag.BoolVar(&ocFlag, "oc", false, "operator count")
	flag.IntVar(&slFlag, "sl", 10000, "riscal statement count limit (mega stmts)")
	flag.BoolVar(&teFlag, "te", false, "trace procedure entry")
	flag.BoolVar(&tiFlag, "ti", false, "trace instructions")
	flag.BoolVar(&toFlag, "to", false, "trace osint calls")
	flag.BoolVar(&trFlag, "tr", false, "trace registers")
	flag.BoolVar(&tsFlag, "ts", false, "trace statements")
	flag.BoolVar(&tcFlag, "tc", false, "trace during compilation")

	flag.Parse()

	if flag.NArg() == 0 {
		fmt.Println("argument file required")
		return
	}
	slFlag = 5000 // convert to statement limit

	fmt.Println("Flags", "sl", slFlag, "oc", ocFlag, "te", teFlag, 
		"tc", tcFlag, "ti", tiFlag,
		"to", toFlag, "tr", trFlag, "ts", tsFlag)
	timeStart = time.Now()
	startup()
	//fmt.Println("stackStart", stackStart)
	interp()

	timeEnd := time.Now()
	elapsed := timeEnd.Sub(timeStart)
	fmt.Println()
	fmt.Println("Execution time", elapsed.Milliseconds(), "ms")
	fmt.Println("Minimal statements executed", stmtCount)
	fmt.Println("Machine instructions executed", instCount)
	//	fmt.Println("Maximum offset", maxOffset)
	fmt.Println("start dynamic", mem[dnamp])
	fmt.Println("end dynamic", mem[dname])
	closefiles()

	finis()
	if interpError {
		panic("abnormal termination")
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}
func finis() {
	/*
		for n,s := range prc_names {
			fmt.Println(n,s)
		}
		os.Exit(0)
	*/
	// list ops not used

/*
// can do all this in sbl, no need for go code here
	if ocFlag {
		var i, nused int
		fmt.Println()
		fmt.Println("ops not used")
		for opn, opc := range opCount {
			if opc == 0 {
				fmt.Println(opn)
			}
		}
		fmt.Println("operation count")
		for opn, opc := range opCount {
			if opc > 0 {
				nused++
				fmt.Println(opn, opc)
			}
		}
		s := make([int]string, nused)
		//		used := make(map[int]string)
		for opn, opc := range opCount {
			if opc > 0 {
				src[i] = opn
				i++
			}

		}
		sort.Strings(s)
		fmt.Println(s)
	}
 */
}
